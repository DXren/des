﻿
// DeosDlg.h: файл заголовка
//

#pragma once
#include <vector>


// Диалоговое окно CDeosDlg
class CDeosDlg : public CDialogEx
{
// Создание
public:
	CDeosDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DEOS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangeEdit1();
	CString sms;
	CString sif;
	CString decod;
	CString key;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedCancel();

	void String_to_BOOL();
	void String_cipher_to_BOOL();
	void key_to_BOOL();
	//void create_keys();

	std::vector <bool> text;
	std::vector <bool> shifr_text;
	std::vector <bool> Key;
	std::vector <bool> T;
	std::vector <bool> decoder_text;
	//bool Var;

	bool keys_48bit[16][48];
	bool C[17][28];
	bool D[17][28];
	afx_msg void OnBnClickedButton3();
};
