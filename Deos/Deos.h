﻿
// Deos.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CDeosApp:
// Сведения о реализации этого класса: Deos.cpp
//

class CDeosApp : public CWinApp
{
public:
	CDeosApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CDeosApp theApp;
